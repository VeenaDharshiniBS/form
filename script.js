const form = document.querySelector("#register");
const usersContainer = document.querySelector("[data-users-container]");

form.addEventListener("submit", (event) => {
  event.preventDefault();
  const formData = new FormData(form);
  const data = Object.fromEntries(formData);
  fetch("http://localhost:3333/users", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).catch((err) => console.error(err));
});

fetch("http://localhost:3333/users")
  .then((res) => res.json())
  .then((users) => {
    users.forEach((user) => {
      let userDiv = document.createElement("div");
      userDiv.innerHTML = `<div>${user.name}</div>
                      <div>${user.email}</div>`;
      usersContainer.append(userDiv);
    });
  })
  .catch((err) => console.error(err));
